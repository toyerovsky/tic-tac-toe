package com.tictactoe;

import javafx.geometry.Pos;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.util.Duration;
/** setting up view of X and Y*/
public class Tile extends StackPane {
    private Text text = new Text();

    public Tile(double width, double height) {
        this.setWidth(width);
        this.setHeight(height);
        Rectangle border = new Rectangle(width, height);
        border.setFill(null);
        border.setStroke(Color.BLACK);
        setAlignment(Pos.CENTER);
        getChildren().addAll(border, text);
    }

    public double getCenterX() {
        return getTranslateX();
    }

    public double getCenterY() {
        return getTranslateY();
    }

    public String getValue() {
        return text.getText();
    }

    public void drawX() {
        text.setText("X");
        TextSizeTransition trans = new TextSizeTransition(text, 80, 100, Duration.millis(150));
        trans.play();
    }

    public void drawO() {
        TextSizeTransition trans = new TextSizeTransition(text, 80, 100, Duration.millis(150));
        text.setText("O");
        trans.play();
    }

    @Override
    public String toString() {
        return super.toString() + " X: " + getCenterX() + " Y: " + getCenterY();
    }
}