package com.tictactoe;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

    Scene scene1;

    public static void main(String[] args) {

        launch(args);
    }
/** Setting up stage and scenes*/
private Stage primaryStage;
    @Override
    public void start(Stage primaryStage) throws Exception{

        Parent root = FXMLLoader.load(getClass().getResource("game.fxml"));
        primaryStage.setTitle("Tic Tac Toe");

        Label label1= new Label("Tic Tac Toe game!");
        Button button1=new Button("Start");
//        button1.setOnAction(e ->primaryStage.setScene(new Scene(root)));

        VBox layout1=new VBox(20);
        layout1.getChildren().addAll(label1,button1);
        scene1=new Scene(layout1,500,500);

        primaryStage.setScene(new Scene(root, 900, 900));
    //    primaryStage.setScene(scene1);
        TicTacToeController ttc=new TicTacToeController();
        if(ttc.checkState()==1){
            primaryStage.setScene(new Scene(FXMLLoader.load(getClass().getResource("EndSceneX.fxml"))));
        }
        else if(ttc.checkState()==0){
            primaryStage.setScene(new Scene(FXMLLoader.load(getClass().getResource("EndSceneO.fxml"))));
        }
        primaryStage.show();
    }

    }



