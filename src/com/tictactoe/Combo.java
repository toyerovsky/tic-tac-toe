package com.tictactoe;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Combo {
    public Tile[] tiles;

    public Combo(Tile... tiles) {
        this.tiles = tiles;
    }

    public boolean isComplete() {
        if (Arrays.stream(tiles).anyMatch(x -> x.getValue().isEmpty()))
            return false;

        Set<String> s = new HashSet<>(
                Arrays.asList(
                        Arrays.stream(tiles)
                                .map(Tile::getValue).toArray(String[]::new)));
        return s.size() == 1;
    }
}