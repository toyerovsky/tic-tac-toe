package com.tictactoe.dao;

import com.tictactoe.model.AccountModel;

import java.sql.*;

public class AccountDao implements AutoCloseable {
    private final String GET_BY_LOGIN_SQL = "SELECT * FROM account WHERE login = ?";
    private final String INSERT_SQL = "INSERT INTO account (login, password_md5_hash, password_salt, description, creation_date) VALUES (?, ?, ?, ?, ?)";
    private final String GET_LAST_ID_SQL = "SELECT MAX(id) FROM account";

    private Connection connection;

    public AccountDao(Connection connection) {
        this.connection = connection;
    }

    public long registerAccount(AccountModel account) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(INSERT_SQL)) {
            statement.setString(1, account.getLogin());
            statement.setString(2, account.getPasswordMd5Hash());
            statement.setString(3, account.getPasswordSalt());
            statement.setString(4, account.getDescription());
            statement.setString(5, account.getCreationDate().toString());
            statement.execute();

            ResultSet rs;
            try (PreparedStatement lastIdStatement = connection.prepareStatement(GET_LAST_ID_SQL)) {
                rs = lastIdStatement.executeQuery();
            }

            return rs.getLong("id");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public AccountModel getAccountByLogin(String login) throws SQLException {
        try (PreparedStatement statement = connection.prepareStatement(GET_BY_LOGIN_SQL)) {
            statement.setString(1, login);
            ResultSet rs = statement.executeQuery();

            AccountModel result = new AccountModel();
            while (rs.next()) {
                result.setId(rs.getLong("id"));
                result.setLogin(rs.getString("login"));
                result.setPasswordMd5Hash(rs.getString("password_me5_hash"));
                result.setPasswordSalt(rs.getString("password_hash"));
                result.setDescription(rs.getString("description"));
                result.setCreationDate(rs.getDate("creation_date"));
            }
            rs.close();
            return result;

        } catch (Exception se) {
            se.printStackTrace();
        }

        return null;
    }

    @Override
    public void close() throws Exception {
        connection.close();

        if (connection != null)
            connection.close();
    }
}

