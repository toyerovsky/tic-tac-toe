package com.tictactoe.dao;

public class Constants {
    public static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    public static final String DB_URL = "jdbc:mysql://localhost/tictactoe";
    public static final String USER = "postgres";
    public static final String PASS = "postgres";
}
