package com.tictactoe;

import java.net.URL;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;

public class TicTacToeController implements Initializable {
    @FXML
    private Pane rootPane;

    private Boolean playable = true;
    private Boolean turnX = true;

    private static final int size = 3;

    private Tile[][] board = new Tile[size][size];
    private List<Combo> combos = new ArrayList<>();

    public int checkState() {

        for (Combo combo : combos) {
            if (combo.isComplete()) {
                playable = false;
                        //                playWinAnimation(combo);

            }
        }
        if(turnX){
            return 0;
        }
        else{

            return 1;
        }

    }

//    /**
//     * drowing lane
//     */
//    private void playWinAnimation(Combo combo) {
//        Line line = new Line();
//
//
//
//        line.setStartX(combo.tiles[0].getCenterX());
//        line.setStartY(combo.tiles[0].getCenterY());
//        line.setEndX(combo.tiles[0].getCenterX());
//        line.setEndY(combo.tiles[0].getCenterY());
//
//
//        rootPane.getChildren().add(line);
//
//        Timeline timeline = new Timeline();
//        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(2),
//                new KeyValue(line.endXProperty(), combo.tiles[2].getCenterX()),
//                new KeyValue(line.endYProperty(), combo.tiles[2].getCenterY())));
//        timeline.play();
//    }

    /**
     * Drowing "X" and "Y" after mouse click
     */
    @Override
    @FXML
    public void initialize(URL location, ResourceBundle resources) {
        double prefPaneSize = 900d;
        rootPane.setPrefSize(prefPaneSize, prefPaneSize);

        double sizeOfTile = prefPaneSize / size;

        double startX = -(sizeOfTile * ((size - 1) / 2d));
        double currentX = startX;
        double currentY = (sizeOfTile * ((size - 1) / 2d));

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                Tile tile = new Tile(sizeOfTile, sizeOfTile);
                tile.setTranslateX(currentX);
                tile.setTranslateY(currentY);

                tile.setOnMouseClicked(event -> {
                    if (!playable)
                        return;

                    if (event.getButton() == MouseButton.PRIMARY) {
                        if (!turnX)
                            return;

                        tile.drawX();
                        turnX = false;
                        checkState();
                    } else if (event.getButton() == MouseButton.SECONDARY) {
                        if (turnX)
                            return;

                        tile.drawO();
                        turnX = true;
                        checkState();
                    }
                });

                rootPane.getChildren().add(tile);

                board[j][i] = tile;
                currentX += sizeOfTile;
            }
            currentX = startX;
            currentY -= sizeOfTile;
        }
/** Possible combinations*/
        // horizontal
        for (int y = 0; y < 3; y++) {
            combos.add(new Combo(board[0][y], board[1][y], board[2][y]));
        }

        // vertical
        for (int x = 0; x < 3; x++) {
            combos.add(new Combo(board[x][0], board[x][1], board[x][2]));
        }

        // diagonals
        combos.add(new Combo(board[0][0], board[1][1], board[2][2]));
        combos.add(new Combo(board[2][0], board[1][1], board[0][2]));
    }
}