package com.tictactoe.service;

import com.tictactoe.dao.AccountDao;
import com.tictactoe.model.AccountModel;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Calendar;

public class AccountService {
    private AccountDao accountDao;

    public AccountService(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    public boolean login(String login, String password) throws NoSuchAlgorithmException {
        AccountModel accountModel = null;
        try {
            accountModel = accountDao.getAccountByLogin(login);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (accountModel != null) {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update((password + accountModel.getPasswordSalt()).getBytes());
            byte[] digest = md.digest();
            String passwordHash = DatatypeConverter
                    .printHexBinary(digest).toUpperCase();

            return accountModel.getPasswordMd5Hash().equals(passwordHash);
        }

        return false;
    }

    public long register(String login, String password, String description) throws NoSuchAlgorithmException, SQLException {
        String salt = generateSalt(10);

        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update((password + salt).getBytes());
        byte[] digest = md.digest();
        String passwordHash = DatatypeConverter
                .printHexBinary(digest).toUpperCase();

        AccountModel accountModel = new AccountModel(login, passwordHash, salt,
                description, new Date(Calendar.getInstance().getTimeInMillis()));

        return accountDao.registerAccount(accountModel);
    }

    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    public static String generateSalt(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }
}
