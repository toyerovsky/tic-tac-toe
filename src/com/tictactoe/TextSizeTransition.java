package com.tictactoe;

import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;


public class TextSizeTransition extends Transition {
    private Text UIcontrol; // a little generic -> subclasses: ButtonBase, Cell, Label, TitledPane
    private int start, end; // initial and final size of the text

    public TextSizeTransition(Text UIcontrol, int start, int end, Duration duration) {
        this.UIcontrol = UIcontrol;
        this.start = start;
        this.end = end; // minus start because of (end * frac) + start in interpolate()
        setCycleDuration(duration);
        setInterpolator(Interpolator.LINEAR);
        //setCycleCount(100);
        // and a lot of other methods
    }

    @Override
    protected void interpolate(double frac) {

        // Determine which is less/greater value between the initial and
        // ending value, then get their difference. This is to resolve the
        // end value when we either increase or decrease the initial value.
        double min = Math.min(start, end); // returns the smaller value
        double max = Math.max(start, end); // returns the higher value
        double diff = max - min; // the positive difference between the two value.

        // Since our start will always be the initial value regardless if
        // it is greater or less than the end, we will simply increase or
        // decrease its value until it reaches the desired end value.
        // Also, computing the difference between their value was to determine
        // if they both have equal values, it means (max - min) is always 0
        // as well as multiplying it to frac, therefore, there will be a
        // 0 value to increase or decrease.
        double size = (start == min) ?
                start + (diff * frac) : // start is smaller so we'll just increase its value
                start - (diff * frac); // decrease if it has the higher value than end


        UIcontrol.setFont(Font.font(size));
    }
}
