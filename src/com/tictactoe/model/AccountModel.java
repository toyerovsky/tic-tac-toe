package com.tictactoe.model;

import java.sql.Date;

public class AccountModel {
    private long id;
    private String login;
    private String passwordMd5Hash;
    private String passwordSalt;
    private String description;
    private Date creationDate;

    public AccountModel() {
    }

    public AccountModel(long id, String login, String passwordMd5Hash, String passwordSalt,
                        String description, Date creationDate) {
        this.id = id;
        this.login = login;
        this.passwordMd5Hash = passwordMd5Hash;
        this.passwordSalt = passwordSalt;
        this.description = description;
        this.creationDate = creationDate;
    }

    public AccountModel(String login, String passwordMd5Hash, String passwordSalt,
                        String description, Date creationDate) {
        this.login = login;
        this.passwordMd5Hash = passwordMd5Hash;
        this.passwordSalt = passwordSalt;
        this.description = description;
        this.creationDate = creationDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPasswordSalt() {
        return passwordSalt;
    }

    public void setPasswordSalt(String passwordSalt) {
        this.passwordSalt = passwordSalt;
    }

    public String getPasswordMd5Hash() {
        return passwordMd5Hash;
    }

    public void setPasswordMd5Hash(String passwordMd5Hash) {
        this.passwordMd5Hash = passwordMd5Hash;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
