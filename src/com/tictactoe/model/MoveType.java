package com.tictactoe.model;

public enum MoveType {
    X, O
}
