package com.tictactoe.model;

import java.sql.Date;

public class GameStepModel {
    private long id;
    private int col;
    private int row;
    private MoveType moveType;
    private Date creationDate;
    private long gameId;

    public GameStepModel(long id, int col, int row, MoveType moveType, Date creationDate, long gameId) {
        this.id = id;
        this.col = col;
        this.row = row;
        this.moveType = moveType;
        this.creationDate = creationDate;
        this.gameId = gameId;
    }

    public long getGameId() {
        return gameId;
    }

    public void setGameId(long gameId) {
        this.gameId = gameId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public MoveType getMoveType() {
        return moveType;
    }

    public void setMoveType(MoveType moveType) {
        this.moveType = moveType;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
