package com.tictactoe.model;

import java.sql.Date;

public class GameModel {
    private long id;
    private Date creationDate;
    private long xPlayer;
    private long oPlayer;
    private long lastStep;

    public GameModel(long id, Date creationDate, long xPlayer, long oPlayer, long lastStep) {
        this.id = id;
        this.creationDate = creationDate;
        this.xPlayer = xPlayer;
        this.oPlayer = oPlayer;
        this.lastStep = lastStep;
    }

    public long getLastStep() {
        return lastStep;
    }

    public void setLastStep(long lastStep) {
        this.lastStep = lastStep;
    }

    public long getoPlayer() {
        return oPlayer;
    }

    public void setoPlayer(long oPlayer) {
        this.oPlayer = oPlayer;
    }

    public long getxPlayer() {
        return xPlayer;
    }

    public void setxPlayer(long xPlayer) {
        this.xPlayer = xPlayer;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
