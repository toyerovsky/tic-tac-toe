import sys
import os
import psycopg2


class CommonException(Exception):
    pass


class Database(object):
    _cursor = None
    _conn = None

    def __init__(self, host, db_name, user, password):
        conn_str = self._get_connection_str(host, db_name, user, password)

        try:
            self._conn = psycopg2.connect(conn_str)
            self._cursor = self._conn.cursor()

        except Exception, e:
            raise CommonException('Unable to initialize connection with database: ' + str(e))

    def get_cursor(self):
        return self._cursor
    
    def commit(self):
        self._conn.commit()

    def close(self):
        self._cursor.close()
        self._conn.close()

    @staticmethod
    def _get_connection_str(host, db_name, user, password):
        return "host='%s' dbname='%s' user='%s' password='%s'" % (host, db_name, user, password)


class Patcher(object):
    _db = None
    _path = None
    _scripts = None

    def __init__(self, db, path):
        self._db = db
        self._path = path
    
        if not os.path.isdir(path):
            raise CommonException('Directory "%s" does not exist' % path)
        
        self._scripts = self._collect_scripts()
        self._validate_script_numbers()
    
    def run(self):
        if len(self._scripts) == 0:
            raise CommonException('No SQL patches available in the given directory')

        self._create_patch_table_if_not_exists()

        last_applied_num = self._get_last_applied_patch_num()
        if last_applied_num is None:
            last_applied_num = self._scripts[0].get_number()
    
        last_script_num = self._scripts[-1].get_number()
        diff = last_script_num - last_applied_num

        if diff >= 1:
            print 'Preparing to apply %d missing %s' % (diff, 'patch' if diff == 1 else 'patches')
            self._apply_scripts(last_applied_num + 1, last_script_num)

        else:
            print 'Database is up to date (%d patches)' % last_applied_num
    
    def _apply_scripts(self, num_from, num_to):
        print ''

        for script in self._scripts[num_from - 1 : num_to]:
            print 'Applying: %s - ' % script,

            try:
                self._execute_patch_sql(script.get_content())
                print 'OK'
            
            except Exception, e:
                print 'FAIL'
                raise CommonException('Unable to execute SQL: ' + str(e))

        print '\nDatabase is now up to date!'

    def _collect_scripts(self):
        scripts = []

        for name in os.listdir(self._path):
            path = os.path.join(self._path, name)

            if os.path.isfile(path):
                script_info = ScriptInfo(path)
                scripts.append(script_info)
        
        return sorted(scripts)
    
    def _validate_script_numbers(self):
        if len(self._scripts) == 0:
            return
        
        for i in range(1, len(self._scripts) - 1):
            curr_script = self._scripts[i]
            prev_script = self._scripts[i - 1]

            if curr_script.get_number() - prev_script.get_number() != 1:
                raise CommonException('SQL patchs are improperly numbered ("%s" > "%s")' % (prev_script, curr_script))
    
    def _get_last_applied_patch_num(self):
        db_cursor = self._db.get_cursor()
        db_cursor.execute('SELECT max(patch_num) FROM db_patch;')
        res = db_cursor.fetchone()[0]

        return int(res) if res is not None else None

    def _create_patch_table_if_not_exists(self):
        db_cursor = self._db.get_cursor()
        db_cursor.execute("CREATE TABLE IF NOT EXISTS db_patch ( patch_num bigint NOT NULL );")
    
    def _execute_patch_sql(self, sql):
        db_cursor = self._db.get_cursor()
        db_cursor.execute(sql)
        db.commit()


class ScriptInfo(object):
    _full_path = None
    _number = None
    _description = None

    def __init__(self, path):
        self._full_path = path
        file_name = os.path.split(path)[-1]

        try:
            self._number, self._description = self._parse_file_name(file_name)
        except:
            raise CommonException('Invalid file name of the SQL patch: "%s"' % file_name)
    
    def __lt__(self, other):
        return self._number < other._number
    
    def __str__(self):
        return '%d - "%s"' % (self._number, self._description)
    
    def get_number(self):
        return self._number
    
    def get_content(self):
        with open(self._full_path) as f:
            return f.read()

    @staticmethod
    def _parse_file_name(file_name):
        file_name_parts = file_name.split('_', 1)
        number = int(file_name_parts[0])
        desc = os.path.splitext(file_name_parts[1])[0]

        return number, desc


if __name__ == '__main__':
    NUM_ARGS = 5

    if len(sys.argv) < NUM_ARGS + 1:
        print 'Invalid arguments, usage: [db host] [db name] [db user] [db password] [path]'
        sys.exit(1)
    
    db_host = sys.argv[1]
    db_name = sys.argv[2]
    db_user = sys.argv[3]
    db_pass = sys.argv[4]
    path = sys.argv[5]

    try:
        print 'Initializing database connection'
        db = Database(db_host, db_name, db_user, db_pass)

        print 'Initializing patcher'
        patcher = Patcher(db, path)

        print 'Running patcher'
        patcher.run()

    except CommonException, e:
        print '\nError: ' + str(e)
        sys.exit(1)