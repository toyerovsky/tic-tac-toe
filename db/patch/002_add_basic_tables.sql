CREATE TABLE account
(
    id                bigserial primary key,
    login             varchar,
    password_md5_hash varchar,
    password_salt     varchar,
    description       varchar,
    creation_date     timestamp with time zone
);

CREATE TABLE game_step
(
    id            bigserial primary key,
    col           int,
    row           int,
    type          varchar,
    creation_date timestamp with time zone
);

CREATE TABLE game
(
    id            bigserial primary key,
    creation_date timestamp with time zone,
    x_player      bigint references account (id),
    o_player      bigint references account (id),
    last_step     bigint references game_step (id)
);

ALTER TABLE game_step
    ADD COLUMN game_id bigint references game (id);

INSERT INTO db_patch
VALUES (2);