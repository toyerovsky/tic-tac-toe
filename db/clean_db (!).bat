@echo off
echo Recreating tictactoe database...
set PGPASSWORD=postgres
dropdb -U postgres tictactoe
createdb -U postgres tictactoe
PAUSE